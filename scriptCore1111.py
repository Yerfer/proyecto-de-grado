"""scriptCoreIndiviual.py"""
# -*- coding: utf-8 -*- #AVERIGUAR PARA QUE FUNCIONE LAS TILDES Y ENYES XD
import commands
import re
import findspark
findspark.init()
import pyspark
from pyspark import SparkContext
import operator 

if __name__ == "__main__":
	def actualizarOld(keysDocsUPDATE):
		print "Actualizando el archivo keysDocs en OldKeysDocs: "
		rta = commands.getstatusoutput('hdfs dfs -rm -R /YERFER/temporal/prueba/reuterspruebaCORE/OldKeysDocs/')
		#El error 256 representa que esta tratando de eliminar un directorio que no existe, si no existe pues lo crea y ya.
		if rta[0] ==  0 or rta[0] ==  256:
			keysDocsUPDATE.coalesce(1).saveAsTextFile("hdfs://Master:9000/YERFER/temporal/prueba/reuterspruebaCORE/OldKeysDocs")
			print ("Actualizacion terminada.")
		else:
			print "Error: "+str(rta[0])+"\nDescripcion: "+rta[1]+" ."

	def unir(palabra):
		palabra=palabra+" ,"
		return palabra

	"""
	#La idea es que se detecte que existe previamente un Spark Context y la detenga para poder iniciar este script 
	#y crear el spark context correspondiente.
	if SparkContext().sparkUser()!=None:
		SparkContext().stop()
		pass"""
	
	sc = SparkContext(appName="Creating Core")

	keysDocs = sc.textFile("hdfs://Master:9000/YERFER/temporal/prueba/reuterspruebaCORE/KeysDocs/part-00000")
	oldKeysDocs = sc.textFile("hdfs://Master:9000/YERFER/temporal/prueba/reuterspruebaCORE/OldKeysDocs/part-00000")
	nuevosDocs = keysDocs.subtract(oldKeysDocs)

	if nuevosDocs.count()==0:
		print "No hay nuevos documentos para cargar T_T"
	else:		
		print "Hay nuevos documentos para cargar!!!"
		print nuevosDocs.collect()

		for pathDocumento in nuevosDocs.collect():

			print "--------------------------------------"

			documento = sc.textFile(pathDocumento)

			#Obtener el nombre del archivo por split
			nombreVector = pathDocumento.split("/")
			nombre = nombreVector[len(nombreVector)-1]
			print "DOCUMENTO: "+nombre

			#Obtener el nombre del archivo por RE
			patronNombre = re.compile('(.*)(\/)')
			nombre2 = re.sub(patronNombre, "",pathDocumento) 
			#print "Nombre del archivo usando ER: "+nombre2		

			print "--------------------------------------"	

			print "cantidad de lineas "+str(documento.count())

			patron1=re.compile('\s+')
			palabrasRDD = documento.flatMap(lambda line : patron1.split(line)).filter(lambda x : len(x) > 0)
			output = palabrasRDD.collect()
			print "cantidad de palabras no limpias "+str(len(output))

			patron2=re.compile('([^a-zA-Z]+\.)|(^\.|\.$|(\s+\.\s+))|(([^a-zA-Z.]))')
			patronURLSValidas = re.compile('(^(www.|(ht|f)tps?:\/\/)((\w*\.\w*)(.*)+)([^\.(\s*|\n*)+]))')
			patronQuitarCharInv = re.compile('([^a-zA-Z]+)|((ht|f)tps?|www)|(\.+(com?|org|info|gov)\.*)|([^\w]+\w[^\w]+)')
			patronSpace = re.compile('\s+')
			palabraslimpias = palabrasRDD.map(lambda word : re.sub(patronQuitarCharInv, " ",word.lower())+" ").filter(lambda x : len(x) > 1).flatMap(lambda line : patron1.split(line)).filter(lambda x : len(x) > 0)#.map(lambda x : x+" ").filter(lambda x : len(x) > 2)
			
			#Cargando el Stopword desde el HDFS.
			stopword = sc.textFile('hdfs://Master:9000/YERFER/temporal/prueba/reuterspruebaCORE/StopWordsEnglish/english.txt')

			print "cantidad de palabras limpias "+str(len(palabraslimpias.collect()))
			print "Cantidad de Stopword "+str(stopword.count())

			interseccion = palabraslimpias.intersection(stopword)
			print "Cantidad de palabras que se van a quitar por el stop words: "+str(interseccion.count())

			print "Palabras que se van a quitar:"
			print interseccion.collect()

			print "Quitando  Palabras..."
			palabrasfiltradas = palabraslimpias.subtract(stopword)

			print "Cantidad de palabras Validas: "+str(palabrasfiltradas.count())


			print "Uniendo Palabras Validas en una sola linea"
			print "Palabras Finales del documento CON REPETIDAS (Muestra)"
			docfinal = palabrasfiltradas.map(lambda x : x+" ").reduce(operator.concat)
			print docfinal[:200]

			print "Palabras Finales del documento SIN REPETIDAS (Muestra)"
			docfinalsinRepetir = palabrasfiltradas.distinct().map(lambda x : x+" ").reduce(operator.concat)
			print docfinalsinRepetir[:200]

			rta = commands.getstatusoutput("hdfs dfs -rm -R /YERFER/temporal/prueba/reuterspruebaCORE/ReutersCore/"+nombre+"")
			#El error 256 representa que esta tratando de eliminar un directorio que no existe, si no existe pues lo crea y ya.
			if rta[0] ==  0 or rta[0] ==  256:
				sc.parallelize([docfinal]).coalesce(1).saveAsTextFile("hdfs://Master:9000/YERFER/temporal/prueba/reuterspruebaCORE/ReutersCore/"+nombre)
				print "----    *************      ------"
				print ("Creacion del Documento filtrado terminado. ")
				print "----    *************      ------"
			else:
				print "----    *************      ------"
				print ("Error: "+str(rta[0])+"\nDescripcion: "+rta[1]+" .")
				print "----    *************      ------"

			
			print "\n"
	if nuevosDocs.count()!=0:
		actualizarOld(keysDocs)		
	else:		
		print "Archivo keysDocs en OldKeysDocs actualizado."
	
	print "\t FIN "
	sc.stop()